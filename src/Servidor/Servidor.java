package Servidor;

import java.awt.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.swing.*;

class Servidor extends JFrame
{
   JTextArea txaMostrar;
   public Servidor()
   {
      super("Consola servidor");
      txaMostrar=new JTextArea();      
    
      this.setContentPane(new JScrollPane(txaMostrar));
      setSize(350,350);
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      setVisible(true);      
      
   }
   public void mostrar(String msg)
   {
      txaMostrar.append(msg+"\n");
   }
   public void runServer()
   {
      ServerSocket ss=null;
      ServerSocket ss2=null;
      boolean listening=true;
      try{
         ss=new ServerSocket(8081);
         ss2=new ServerSocket(8082);
         mostrar("SERVIDOR ON");
         while(listening)
         {
            Socket sock=null,sock2=null;
            try {
               mostrar("Esperando usuarios para que se conecten");
               sock=ss.accept();
               sock2=ss2.accept();
            } catch (IOException e)
            {
               mostrar("Error: " + ss + ", " + e.getMessage());
               continue;
            }
            threadServidor user=new threadServidor(sock,sock2,this);            
	    user.start();
         }
         
      }catch(IOException e){
         mostrar("Error :"+e);
      }
   }
   
   public static void main(String abc[]) throws IOException
   {                
     Servidor ser= new Servidor();
     ser.runServer();
   }
}



