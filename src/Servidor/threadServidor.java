package Servidor;
import java.io.*;
import java.net.*;
import java.util.*;

public class threadServidor extends Thread
{
     Socket scli=null;
     Socket scli2=null;
     DataInputStream entrada=null;
     DataOutputStream salida=null;
     DataOutputStream salida2=null;
     public static Vector<threadServidor> clientesActivos=new Vector();	
     String user;
     Servidor serv;
     
     public threadServidor(Socket scliente,Socket scliente2,Servidor serv)
     {
        scli=scliente;
        scli2=scliente2;
        this.serv=serv;
        user ="";
        clientesActivos.add(this);        
        serv.mostrar("Usuario conectado: "+this);
     }
     
     public String getUser()
     {
       return user;
     }
     
     public void setUser(String name)
     {
       user =name;
     }
     
     public void run()
     {
    	serv.mostrar("Esperando comunicacion entre usuarios :");
    	
    	try
    	{
          entrada=new DataInputStream(scli.getInputStream());
          salida=new DataOutputStream(scli.getOutputStream());
          salida2=new DataOutputStream(scli2.getOutputStream());
          this.setUser(entrada.readUTF());
          enviaUserActivos();
    	}
    	catch (IOException e) {  e.printStackTrace();     }
    	
        int opcion=0,numUsers=0;
        String amigo="",mencli="";
                
    	while(true)
    	{
          try
          {
             opcion=entrada.readInt();
             switch(opcion)
             {
                case 1:
                   mencli=entrada.readUTF();
                   serv.mostrar("Mensaje recibido "+mencli);
                   enviaMsg(mencli);
                   break;
                case 2:
                   numUsers=clientesActivos.size();
                   salida.writeInt(numUsers);
                   for(int i=0;i<numUsers;i++)
                      salida.writeUTF(clientesActivos.get(i).user);
                   break;
                case 3:
                   amigo=entrada.readUTF();
                   mencli=entrada.readUTF();
                   enviaMsg(amigo,mencli);
                   break;
             }
          }
          catch (IOException e) {System.out.println("El cliente termino la conexion");break;}
    	}
    	try
    	{
          serv.mostrar("Se desconecto un Usuario");
          scli.close();
    	}	
        catch(Exception et)
        {serv.mostrar("No se puede cerrar el socket");}
     }
     
     public void enviaMsg(String mencli2)
     {
        threadServidor user=null;
        for(int i=0;i<clientesActivos.size();i++)
        {
           serv.mostrar("MENSAJE DEVUELTO: "+mencli2);
           try
            {
              user=clientesActivos.get(i);
              user.salida2.writeInt(1);
              user.salida2.writeUTF(""+this.getUser()+" >"+ mencli2);
            }catch (IOException e) {e.printStackTrace();}
        }
     }
     public void enviaUserActivos()
     {
        threadServidor user=null;
        for(int i=0;i<clientesActivos.size();i++)
        {           
           try
            {
              user=clientesActivos.get(i);
              if(user==this)continue;
              user.salida2.writeInt(2);
              user.salida2.writeUTF(this.getUser());
            }catch (IOException e) {e.printStackTrace();}
        }
     }

   private void enviaMsg(String amigo, String mencli) 
   {
      threadServidor user=null;
        for(int i=0;i<clientesActivos.size();i++)
        {           
           try
            {
              user=clientesActivos.get(i);
              if(user.user.equals(amigo))
              {
                 user.salida2.writeInt(3);   
                 user.salida2.writeUTF(this.getUser());
                 user.salida2.writeUTF(""+this.getUser()+"> "+mencli);
              }
            }catch (IOException e) {e.printStackTrace();}
        }
   }
}