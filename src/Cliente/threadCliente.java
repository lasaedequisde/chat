package Cliente;
import java.lang.*;
import java.io.*;

class threadCliente extends Thread{
   DataInputStream entrada;
   Ventana_cliente vc;
   public threadCliente (DataInputStream entrada, Ventana_cliente vc) throws IOException
   {
      this.entrada=entrada;
      this.vc = vc;
   }

   public void run()
   {
      String menser="",amigo="";
      int opcion=0;
      while(true)
      {         
         try{
            opcion=entrada.readInt();
            switch(opcion)
            {
               case 1:
                  menser=entrada.readUTF();
                  System.out.println("Servidor:"+menser);
                  vc.mostrarMsg(menser);
                  break;
               case 2:
                  menser=entrada.readUTF();
                  vc.agregarUser(menser);
                  break;
               case 3:
                  amigo=entrada.readUTF();
                  menser=entrada.readUTF();
                  vc.mensageAmigo(amigo, menser);
                  System.out.println("Servidor:"+menser);
                  break;
            }
         }
         catch (IOException e){
            System.out.println("Error");
            break;
         }
      }
      System.out.println("Se desconecto el servidor");
   }

   
}