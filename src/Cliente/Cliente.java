package Cliente;

import java.io.*;
import java.net.*;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class Cliente
{
   public static String ip;
   Ventana_cliente vent;
   DataInputStream entrada = null;
   DataOutputStream salida = null;
   DataInputStream entrada2 = null;
   Socket comunicacion = null;
   Socket comunicacion2 = null;
   
   String user;
   String pass;

   public Cliente(Ventana_cliente vent) throws IOException
   {
      this.vent=vent;
   }

   public void conexion() throws IOException 
   {
      try {
         comunicacion = new Socket(Cliente.ip, 8081);
         comunicacion2 = new Socket(Cliente.ip, 8082);
         entrada = new DataInputStream(comunicacion.getInputStream());
         salida = new DataOutputStream(comunicacion.getOutputStream());
         entrada2 = new DataInputStream(comunicacion2.getInputStream());
         user = JOptionPane.showInputDialog("Introducir Nombre Usuario:");
         pass = JOptionPane.showInputDialog("Introduce contraseña para tu Usuario:");
         vent.setNombreUser(user);
         salida.writeUTF(user);
      } catch (IOException e) {
         System.out.println("El servidor no esta operativo");
      }
      new threadCliente(entrada2, vent).start();
   }
   public String getNombre()
   {
      return user;
   }
   public Vector<String> pedirUsuarios()
   {
      Vector<String> users = new Vector();
      try {         
         salida.writeInt(2);
         int nUsuarios=entrada.readInt();
         for(int i=0;i<nUsuarios;i++)
            users.add(entrada.readUTF());
      } catch (IOException ex) {
         Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
      }
      return users;
   }
   public void flujo(String mens) 
   {
      try {             
         System.out.println("el mensaje enviado desde el cliente es : "
             + mens);
         salida.writeInt(1);
         salida.writeUTF(mens);
      } catch (IOException e) {
         System.out.println("Error...." + e);
      }
   }
   
   public void flujo(String amigo,String mens) 
   {
      try {             
         System.out.println("Mensaje del cliente: "
             + mens);
         salida.writeInt(3);
         salida.writeUTF(amigo);
         salida.writeUTF(mens);
      } catch (IOException e) {
         System.out.println("Error...." + e);
      }
   }
   
  
}
