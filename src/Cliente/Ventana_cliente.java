package Cliente;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Vector;
import javax.swing.*;

public class Ventana_cliente extends JFrame implements ActionListener {
     String mensajeCliente;
     JTextArea chat;
     JTextField tfchat;
     JButton btenviar;
     JLabel luser;
     JList lstActivos;
     JButton btprivado;
     Cliente cliente;
     
     Vector<String> usuarios;
     Ventana_Privada chatPrivado;

     public Ventana_cliente() throws IOException {
             super("Cliente Chat");
             tfchat = new JTextField(30);
             btenviar = new JButton("Enviar");
             luser = new JLabel("Usuario <<  >>");
             luser.setHorizontalAlignment(JLabel.CENTER);
             chat = new JTextArea();
             chat.setColumns(25);
             tfchat.addActionListener(this);
             btenviar.addActionListener(this);
             lstActivos=new JList();             
             btprivado =new JButton("Ventana");
             btprivado.addActionListener(this);

             chat.setEditable(false);

             JPanel panAbajo = new JPanel();
             panAbajo.setLayout(new BorderLayout());
                panAbajo.add(new JLabel("  Mensaje:"),BorderLayout.NORTH);
                panAbajo.add(tfchat, BorderLayout.CENTER);
                panAbajo.add(btenviar, BorderLayout.EAST);
             JPanel panRight = new JPanel();
             panRight.setLayout(new BorderLayout());
                panRight.add(luser, BorderLayout.NORTH);
                panRight.add(new JScrollPane(chat), BorderLayout.CENTER);
                panRight.add(panAbajo,BorderLayout.SOUTH);
             JPanel panLeft=new JPanel();
             panLeft.setLayout(new BorderLayout());
               panLeft.add(new JScrollPane(this.lstActivos),BorderLayout.CENTER);
               panLeft.add(this.btprivado,BorderLayout.NORTH);
             JSplitPane sldCentral=new JSplitPane();  
             sldCentral.setDividerLocation(100);
             sldCentral.setDividerSize(7);
             sldCentral.setOneTouchExpandable(true);
               sldCentral.setLeftComponent(panLeft);
               sldCentral.setRightComponent(panRight);
             
             
             setLayout(new BorderLayout());
             add(sldCentral, BorderLayout.CENTER);
             
             tfchat.requestFocus();
             
             cliente=new Cliente(this);
             cliente.conexion();     
             usuarios =new Vector();
             ponerActivos(cliente.pedirUsuarios());
             
             chatPrivado =new Ventana_Privada(cliente);
                  
             setSize(480, 430);
             setLocation(120, 90);
             setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);				
             setVisible(true);
     }
     
     public void setNombreUser(String user)
     {
        luser.setText("Usuario " + user);
     }
     public void mostrarMsg(String msg)
     {
        this.chat.append(msg + "\n");
     }
     public void ponerActivos(Vector datos)
     {
        usuarios =datos;
        ponerDatosList(this.lstActivos, usuarios);
     }
     public void agregarUser(String user)
     {
        usuarios.add(user);
        ponerDatosList(this.lstActivos, usuarios);
     }
     public void retirraUser(String user)
     {        
        usuarios.remove(user);
        ponerDatosList(this.lstActivos, usuarios);
     }
    private void ponerDatosList(JList list,final Vector datos)
    {
        list.setModel(new AbstractListModel() {            
            @Override
            public int getSize() { return datos.size(); }
            @Override
            public Object getElementAt(int i) { return datos.get(i); }
        });
    }
    @Override
     public void actionPerformed(ActionEvent evt) {

       String comand=(String)evt.getActionCommand();
        if(evt.getSource()==this.btenviar || evt.getSource()==this.tfchat)
        {
           String mensaje = tfchat.getText();
           cliente.flujo(mensaje);
           tfchat.setText("");
        }
        else if(evt.getSource()==this.btprivado)
        {
           int pos=this.lstActivos.getSelectedIndex();
           if(pos>=0)              
           {
              chatPrivado.setAmigo(usuarios.get(pos));
              chatPrivado.setVisible(true);
           }
        }
     }
     
     public void mensageAmigo(String amigo,String msg)
     {
        chatPrivado.setAmigo(amigo);
        chatPrivado.mostrarMsg(msg);
        chatPrivado.setVisible(true);
     }

     public static void main(String args[]) throws IOException {
             Cliente.ip = JOptionPane.showInputDialog("Introducir Dirección IP_Servidor:","127.0.0.1");
             Ventana_cliente p = new Ventana_cliente();
     }
}
