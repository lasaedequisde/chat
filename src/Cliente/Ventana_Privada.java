package Cliente;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.*;

public class Ventana_Privada extends JFrame implements ActionListener
{
   JTextArea chat;
   JTextField tfchat;
   JButton btEnviar;
   
   Cliente cliente;
   String otro_usuario;
   
   public Ventana_Privada(Cliente cliente)
   {
      super("Amigo");
      this.cliente=cliente;
      tfchat = new JTextField(30);
      btEnviar = new JButton("Enviar");
      chat = new JTextArea(); 
      chat.setEditable(false);
      tfchat.requestFocus();
      tfchat.addActionListener(this);
      btEnviar.addActionListener(this);
      
      JPanel panAbajo = new JPanel();
             panAbajo.setLayout(new BorderLayout());
             panAbajo.add(new JLabel("  Mensaje:"),
                                BorderLayout.NORTH);
             panAbajo.add(tfchat, BorderLayout.CENTER);
             panAbajo.add(btEnviar, BorderLayout.EAST);
      
      setLayout(new BorderLayout());
      add(new JScrollPane(chat),BorderLayout.CENTER);
      add(panAbajo,BorderLayout.SOUTH);
       
      otro_usuario ="";
      
      this.addWindowListener(new WindowListener()
      {         
         public void windowClosing(WindowEvent e) {
            cerrarVentana();
         }
         public void windowClosed(WindowEvent e) {}         
         public void windowOpened(WindowEvent e) {}
         public void windowIconified(WindowEvent e) {}
         public void windowDeiconified(WindowEvent e) {}
         public void windowActivated(WindowEvent e) {}
         public void windowDeactivated(WindowEvent e) {}
        
      });
      
      setSize(300,300);
      setLocation(570,90);      			      
   }
   public void setAmigo(String ami)
   {      
      this.otro_usuario =ami;
      this.setTitle(ami);      
   }
    private void cerrarVentana() 
    {       
      this.setVisible(false);      
    }
    public void mostrarMsg(String msg)
     {
        this.chat.append(msg + "\n");
     }
    
   @Override
   public void actionPerformed(ActionEvent e) 
   {
      String mensaje = tfchat.getText();              
      mostrarMsg(cliente.getNombre() + ">" + mensaje);
      cliente.flujo(otro_usuario, mensaje);
      tfchat.setText("");
   }
}
